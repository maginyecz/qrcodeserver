package hitcount

import "sync"

type hc struct {
	hits map[string]int
	lock sync.Mutex
}

func New() *hc {
	return &hc{
		hits: make(map[string]int),
	}
}

func (h *hc) Hit(url string) {
	h.lock.Lock()
	defer h.lock.Unlock()
	h.hits[url]++
}

func (h *hc) All() map[string]int {
	return h.hits
}

func (h *hc) URL(url string) int {
	count, ok := h.hits[url]
	if !ok {
		return 0
	}
	return count
}
