package main

import (
	"bytes"
	"errors"
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	qrcode "github.com/skip2/go-qrcode"
	"gitlab.com/maginyecz/moreinfo/hitcount"
)

const (
	port     = ":8080"
	minSize  = 1200
	maxLogoW = 256
	maxLogoH = 256
)

func main() {
	//hits = make(map[string]int)
	hits := hitcount.New()
	r := gin.Default()

	r.GET("/hits", func(c *gin.Context) {
		c.JSON(http.StatusOK, hits.All())
	})

	r.GET("/", func(c *gin.Context) {
		url := c.DefaultQuery("url", "http://localhost")
		size := c.DefaultQuery("size", strconv.Itoa(minSize))
		hits.Hit(url)
		sizeint, err := strconv.Atoi(size)
		if err != nil || sizeint < minSize {
			c.String(http.StatusBadRequest, "Illegal size")
			return
		}

		png, err := generatePNG(url, sizeint)
		if err != nil {
			fmt.Println(err)
			c.String(http.StatusBadRequest, "Unable to render image")
			return
		}
		png, err = addLogo(png, "./logo.png")
		if err != nil {
			fmt.Println(err)
			c.String(http.StatusBadRequest, "Unable to render image")
			return
		}
		c.Data(200, "image/png", png)
	})
	r.Run(port)
}

func addLogo(input []byte, logofile string) ([]byte, error) {
	imgfile, err := os.Open(logofile)
	if err != nil {
		return nil, err
	}
	defer imgfile.Close()
	logo, err := png.Decode(imgfile)
	if err != nil {
		return nil, err
	}
	basepng, _, _ := image.Decode(bytes.NewReader(input))
	dstimage := image.NewRGBA(image.Rect(0, 0, basepng.Bounds().Dx(), basepng.Bounds().Dy()))
	draw.Draw(dstimage, dstimage.Bounds(), basepng, image.ZP, draw.Src)

	baseW, baseH := dstimage.Bounds().Dx(), dstimage.Bounds().Dy()
	logoW, logoH := logo.Bounds().Dx(), logo.Bounds().Dy()

	if logoW > maxLogoW || logoH > maxLogoH {
		return nil, errors.New("Logo dimensions error")
	}

	// still can't understand why this is the proper zero point
	// one would expect it to be greater than 0
	pt := image.Point{
		X: -(baseW/2 - logoW/2),
		Y: -(baseH/2 - logoH/2),
	}

	draw.Draw(dstimage, dstimage.Bounds(), logo, pt, draw.Over)
	buff := new(bytes.Buffer)
	err = png.Encode(buff, dstimage)
	if err != nil {
		return nil, err
	}
	return buff.Bytes(), nil
}

func generatePNG(url string, size int) ([]byte, error) {
	var png []byte
	png, err := qrcode.Encode(url, qrcode.High, size)
	if err != nil {
		return nil, err
	}
	return png, nil
}
